export CABAL_DIR="${XDG_CACHE_HOME}/cabal/"
export INPUTRC="${XDG_CONFIG_HOME}/readline/inputrc"
export LESSHISTFILE="${XDG_CACHE_HOME}/less/history"
export PERL_MB_OPT="--install_base \"${XDG_DATA_HOME}/perl\""
export _Z_DATA="${XDG_CACHE_HOME}/z"
export GRADLE_USER_HOME="${XDG_DATA_HOME}/gradle"
export GNUPGHOME="${XDG_DATA_HOME}/gnupg"
export LESSKEY="${XDG_CONFIG_HOME}/less/lesskey"
export PATH="${XDG_DATA_HOME}/perl/bin${PATH:+:${PATH}}"
export PERL5LIB="${XDG_DATA_HOME}/perl/lib/perl5${PERL5LIB:+:${PERL5LIB}}"
export PERL_MM_OPT="INSTALL_BASE=${XDG_DATA_HOME}/perl"
export PSQL_HISTORY="${XDG_CACHE_HOME}/psql_history"
export RUSTUP_HOME="${XDG_DATA_HOME}/rustup"
export SOLARGRAPH_CACHE="${XDG_CACHE_HOME}/solargraph"
export CARGO_HOME="${XDG_DATA_HOME}/cargo"
export GOPATH="${XDG_DATA_HOME}/go"
export GTK2_RC_FILES="${XDG_CONFIG_HOME}/gtk-2.0/gtkrc"
export HISTFILE="${XDG_CACHE_HOME}/bash/history"
export PERL_LOCAL_LIB_ROOT="${XDG_DATA_HOME}/perl${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"
export STACK_ROOT="${XDG_DATA_HOME}/stack"
export PARALLEL_HOME="${XDG_CONFIG_HOME}/parallel"
export CABAL_CONFIG="${XDG_CONFIG_HOME}/cabal/config"
export JULIA_DEPOT_PATH="${XDG_DATA_HOME}/julia:${JULIA_DEPOT_PATH}"
export PERL_CPANM_HOME="${XDG_DATA_HOME}/cpanm"
export _JAVA_OPTIONS="-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java"
export npm_config_cache="${XDG_CACHE_HOME}/npm"
