return {
  { "Darazaki/indent-o-matic",              enabled = false },
  { "tpope/vim-surround",                   lazy = false },
  { "rhysd/clever-f.vim",                   lazy = false },
  { "ron-rs/ron.vim" },
  { "Grafcube/bf-vim" },
  { "samirettali/shebang.nvim",             lazy = false },
  { "lervag/vimtex",                        ft = { "tex" } },
  { "davidgranstrom/nvim-markdown-preview", cmd = "MarkdownPreview" },
  { "benknoble/vim-racket" },
  { "rhysd/reply.vim",                      lazy = false },
  { "kshenoy/vim-signature",                lazy = false },
  { "mg979/vim-visual-multi",               lazy = false },
  { "pgdouyon/vim-evanesco",                lazy = false },
  { "ntpeters/vim-better-whitespace",       lazy = false },
  { "jghauser/mkdir.nvim",                  lazy = false },
  { "fidian/hexmode",                       cmd = "Hexmode" },
  { "tpope/vim-sleuth",                     lazy = false },
  { "tpope/vim-fugitive" },
  { "folke/persistence.nvim" },
  { "Lommix/godot.nvim",                    ft = { "gdscript", "gdresource", "gdshader" } },
  { "catppuccin/nvim",                      name = "catppuccin" },
  {
    "alpertuna/vim-header",
    cmd = {
      "AddHeader",
      "AddMinHeader",
      "AddMITLicense",
      "AddApacheLicense",
      "AddGNULicense",
      "AddAGPLicense",
      "AddLGPLLicense",
      "AddMPLLicense",
      "AddWTFPLLicense",
      "AddZlibLicense",
    }
  },
  {
    "simnalamburt/vim-mundo",
    cmd = { "MundoToggle" },
  },
  {
    "rmagatti/auto-session",
    lazy = false,
    opts = {
      auto_session_root_dir = vim.fn.stdpath('state') .. "/sessions/",
      auto_session_suppress_dirs = { "~/", "~/projects", "~/Downloads", "~/Documents", "/" },
      cwd_change_handling = {
        restore_upcoming_session = false,
      }
    }
  },
  {
    "rmagatti/session-lens",
    lazy = false,
    dependencies = { 'rmagatti/auto-session', 'nvim-telescope/telescope.nvim' },
    config = function()
      require('session-lens').setup()
      require("telescope").load_extension("session-lens")
    end
  },
  {
    "mzlogin/vim-markdown-toc",
    cmd = { "GenTocGFM", "GenTocRedcarpet", "GenTocGitLab", "GenTocMarked",
      "UpdateToc", "RemoveToc", }
  },
  {
    "williamboman/mason.nvim",
    opts = {
      PATH = "prepend",
    }
  },
  {
    "nvim-treesitter/nvim-treesitter",
    opts = {
      auto_install = true,
    }
  },
  {
    "max397574/better-escape.nvim",
    opts = {
      mapping = { "jk", "kj", "JK", "KJ" },
      clear_empty_lines = true,
    }
  },
  {
    "jose-elias-alvarez/null-ls.nvim",
    lazy = false,
    opts = function(_, opts)
      local null_ls = require "null-ls"

      local leptosfmt = {
        name = "leptosfmt",
        meta = {
          url = "https://github.com/bram209/leptosfmt",
          description = "A formatter for the leptos view! macro.",
        },
        method = null_ls.methods.FORMATTING,
        filetypes = { "rust" },
        generator = null_ls.formatter({
          command = "leptosfmt",
          args = { "--stdin", "--quiet" },
          to_stdin = true,
          from_stderr = false,
          from_stdout = true,
        }),
      }

      opts.sources = {
        null_ls.builtins.formatting.shfmt.with({
          filetypes = { "sh", "bash", "zsh", "pkgbuild" },
        }),
        null_ls.builtins.formatting.sql_formatter.with({
          extra_args = { "--config", vim.env.XDG_CONFIG_HOME .. "/sql-formatter.json" },
        }),
        null_ls.builtins.formatting.prettier.with({
          filetypes = { "json", "markdown", "yaml", "html", "css" },
        }),
        leptosfmt,
      }

      return opts
    end
  },
  {
    "rebelot/heirline.nvim",
    opts = function(_, opts)
      local status = require("astronvim.utils.status")
      opts.statusline = {
        hl = { fg = "fg", bg = "bg" },
        status.component.mode { mode_text = { padding = { left = 1, right = 1 } } },
        status.component.git_branch(),
        status.component.file_info(),
        status.component.git_diff(),
        status.component.fill(),
        status.component.diagnostics(),
        status.component.lsp(),
        status.component.treesitter { padding = { right = 1 } },
        status.component.file_info {
          filename = false,
          file_modified = false,
          file_read_only = false,
          filetype = {},
        },
        status.component.builder {
          {
            provider = function()
              return vim.o.encoding:upper()
            end
          },
          padding = { right = 2 },
        },
        status.component.builder {
          {
            provider = function()
              local fileformat_symbols = {
                unix = '', -- e712
                dos = '', -- e70f
                mac = '', -- e711
              }
              return fileformat_symbols[vim.api.nvim_buf_get_option(0, "fileformat")]
            end
          },
          padding = { right = 2 },
        },
        status.component.builder {
          {
            provider = function()
              local option = vim.api.nvim_buf_get_option(0, "expandtab")
              return (option and "Spaces: " or "Tabs: ") ..
                  vim.api.nvim_buf_get_option(0, option and "shiftwidth" or "tabstop")
            end
          },
          padding = { right = 1 },
        },
        status.component.nav {
          scrollbar = false,
          padding = { right = 1 },
          surround = { separator = "right", color = status.hl.mode_bg },
          hl = { fg = "bg" },
        },
      }
      return opts
    end,
  },
  {
    "klen/nvim-config-local",
    lazy = false,
    opts = {
      config_files = { ".nvim.lua", ".nvimrc", ".exrc" },
      hashfile = vim.fn.stdpath("cache") .. "/config-local",
      autocommands_create = true,
      commands_create = true,
      silent = false,
      lookup_parents = false,
    }
  },
  {
    "windwp/nvim-autopairs",
    config = function(plugin, opts)
      require("plugins.configs.nvim-autopairs")(plugin, opts)
      local Rule = require "nvim-autopairs.rule"
      local npairs = require "nvim-autopairs"
      local cond = require "nvim-autopairs.conds"
      npairs.add_rules {
        Rule(' ', ' ')
            :with_pair(function(options)
              local pair = options.line:sub(options.col - 1, options.col)
              return vim.tbl_contains({ '()', '{}', '[]' }, pair)
            end)
            :with_move(cond.none())
            :with_cr(cond.none())
            :with_del(function(options)
              local col = vim.api.nvim_win_get_cursor(0)[2]
              local context = options.line:sub(col - 1, col + 2)
              return vim.tbl_contains({ '(  )', '{  }', '[  ]' }, context)
            end),
        Rule('', ' )')
            :with_pair(cond.none())
            :with_move(function(options) return options.char == ')' end)
            :with_cr(cond.none())
            :with_del(cond.none())
            :use_key(')'),
        Rule('', ' }')
            :with_pair(cond.none())
            :with_move(function(options) return options.char == '}' end)
            :with_cr(cond.none())
            :with_del(cond.none())
            :use_key('}'),
        Rule('', ' ]')
            :with_pair(cond.none())
            :with_move(function(options) return options.char == ']' end)
            :with_cr(cond.none())
            :with_del(cond.none())
            :use_key(']'),
      }
    end,
  },
  {
    "weilbith/nvim-code-action-menu",
    cmd = "CodeActionMenu",
  },
  {
    "nvim-neotest/neotest",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-treesitter/nvim-treesitter",
      "nvim-neotest/neotest-python",
      "rouge8/neotest-rust",
    },
    config = function()
      require("neotest").setup({
        adapters = {
          require("neotest-python"),
          require("neotest-rust"),
        },
      })
    end
  },
  {
    "mfussenegger/nvim-dap",
    config = function()
      local dap = require("dap")
      dap.adapters.godot = {
        type = "server",
        host = '127.0.0.1',
        port = 6006,
      }

      dap.configurations.gdscript = {
        {
          type = "godot",
          request = "launch",
          name = "Launch scene",
          project = "${workspaceFolder}",
          launch_scene = true,
        },
      }
    end,
  },
  {
    "machakann/vim-highlightedyank",
    config = function()
      vim.g.highlightedyank_highlight_duration = 400
      vim.g.highlightedyank_highlight_in_visual = 0
    end
  },
  {
    "NvChad/nvim-colorizer.lua",
    lazy = false,
    opts = function(_, _)
      local wt = {
        names = true,                                    -- "Name" codes like Blue or blue
        rgb_fn = true,                                   -- CSS rgb() and rgba() functions
        hsl_fn = true,                                   -- CSS hsl() and hsla() functions
        css = true,                                      -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
        css_fn = true,                                   -- Enable all CSS *functions*: rgb_fn, hsl_fn
        mode = "background",                             -- Set the display mode.
        tailwind = "lsp",                                -- Enable tailwind colors
        sass = { enable = false, parsers = { "css" }, }, -- Enable sass colors
      }
      return {
        user_default_options = {
          RGB = true,          -- #RGB hex codes
          RRGGBB = true,       -- #RRGGBB hex codes
          names = false,       -- "Name" codes like Blue or blue
          RRGGBBAA = true,     -- #RRGGBBAA hex codes
          AARRGGBB = true,     -- 0xAARRGGBB hex codes
          rgb_fn = false,      -- CSS rgb() and rgba() functions
          hsl_fn = false,      -- CSS hsl() and hsla() functions
          css = false,         -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
          css_fn = false,      -- Enable all CSS *functions*: rgb_fn, hsl_fn
          mode = "background", -- Set the display mode.
          tailwind = false,    -- Enable tailwind colors
        },
        filetypes = {
          html = wt,
          css = wt,
          sass = wt,
          scss = wt,
          svelte = wt,
          rust = wt,
        }
      }
    end
  },
  {
    "fedepujol/move.nvim",
    lazy = false,
    config = function()
      local opts = { noremap = true, silent = true }
      -- Normal-mode commands
      vim.keymap.set('n', '<A-j>', ':MoveLine(1)<CR>', opts)
      vim.keymap.set('n', '<A-k>', ':MoveLine(-1)<CR>', opts)
      vim.keymap.set('n', '<A-h>', ':MoveHChar(-1)<CR>', opts)
      vim.keymap.set('n', '<A-l>', ':MoveHChar(1)<CR>', opts)
      -- Visual-mode commands
      vim.keymap.set('v', '<A-j>', ':MoveBlock(1)<CR>', opts)
      vim.keymap.set('v', '<A-k>', ':MoveBlock(-1)<CR>', opts)
      vim.keymap.set('v', '<A-h>', ':MoveHBlock(-1)<CR>', opts)
      vim.keymap.set('v', '<A-l>', ':MoveHBlock(1)<CR>', opts)
    end
  },
  {
    "rcarriga/nvim-notify",
    lazy = false,
    opts = { background_colour = "#000000" },
  },
  {
    "simrat39/rust-tools.nvim",
    lazy = false,
    dependencies = { "mason-lspconfig.nvim" },
    event = "User AstroLspSetup",
    opts = {
      server = require("astronvim.utils.lsp").config("rust_analyzer"),
      tools = {
        inlay_hints = { auto = true },
        hover_actions = { auto_focus = true },
      },
    }
  },
  {
    "ray-x/lsp_signature.nvim",
    config = function()
      require 'lsp_signature'.setup({ hint_prefix = " " })
    end,
  },
  {
    "kosayoda/nvim-lightbulb",
    lazy = false,
    opts = function()
      local text = ""
      return {
        autocmd = {
          enabled = true
        },
        hide_in_unfocused_buffer = true,
        sign = {
          enabled = true,
          text = text,
          hl = "LightBulbSign",
        },
        virtual_text = {
          enabled = false,
          text = text,
          pos = "eol",
          hl = "LightBulbVirtualText",
          hl_mode = "combine",
        },
        float = {
          enabled = false,
          text = text,
          hl = "LightBulbFloatWin",
          win_opts = {
              focusable = false,
          },
        },
        status_text = {
          enabled = false,
          text = text,
          text_unavailable = "",
        },
      }
    end,
  },
  {
    "Xuyuanp/scrollbar.nvim",
    lazy = false,
    config = function()
      vim.api.nvim_create_autocmd(
        { "WinScrolled", "VimResized", "QuitPre", "WinEnter", "FocusGained" }, {
          pattern = "*",
          command = "silent! lua require('scrollbar').show()"
        })
      vim.api.nvim_create_autocmd(
        { "WinLeave", "BufLeave", "BufWinLeave", "FocusLost" }, {
          pattern = "*",
          command = "silent! lua require('scrollbar').clear()"
        })
    end,
  },
  {
    'CRAG666/code_runner.nvim',
    lazy = false,
    dependencies = 'nvim-lua/plenary.nvim',
    config = function()
      require('code_runner').setup({
        mode = "float",
        float = {
          border = "rounded",
        },
        project_path = vim.fn.stdpath('state') .. "/project_manager.json",
        filetype = {
          sh = "$file",
          zig = "zig build-exe $file && $dir/$fileNameWithoutExt",
          c = "cd $dir && gcc -lm $file -o $fileNameWithoutExt && $dir/$fileNameWithoutExt",
          cpp = "cd $dir && g++ $fileName -o $fileNameWithoutExt && $dir/$fileNameWithoutExt",
          rust = "cd $dir && rustc $fileName && $dir/$fileNameWithoutExt",
          perl = "perl $file",
          ruby = "ruby $file",
          python = "python3 $file",
          julia = "julia $file",
          haskell = "ghc $file && $dir/$fileNameWithoutExt",
          racket = "racket $file",
          java = "java $file",
          tcl = "ns $file",
        },
      })
    end,
  },
  {
    "abecodes/tabout.nvim",
    lazy = false,
    config = function()
      require("tabout").setup {
        act_as_shift_tab = true,
      }
    end,
  },
  {
    "JuliaEditorSupport/julia-vim",
    lazy = false,
    config = function()
      vim.g.latex_to_unicode_file_types = { ".*" }
      vim.api.nvim_set_keymap("i", "<C-Tab>", "<C-x><C-u>", { noremap = false, silent = true })
    end,
  },
  {
    "eraserhd/parinfer-rust",
    ft = { "racket", "scheme", "lisp", "yuck" },
    build = "cargo build --release",
    config = function()
      vim.g.parinfer_logfile = vim.env.XDG_CACHE_HOME .. "/nvim/parinfer.log"
      vim.api.nvim_create_autocmd(
        "FileType", {
          pattern = { "racket", "scheme", "lisp", "yuck" },
          command = "ParinferOn"
        })
    end,
  },
  {
    "ggandor/leap.nvim",
    lazy = false,
    dependencies = { "tpope/vim-repeat" },
    config = function()
      require('leap').set_default_keymaps()
      vim.api.nvim_set_keymap("n", "S", "s", { noremap = true, silent = true })
      vim.api.nvim_set_keymap("n", "s",
        ":lua require('leap').leap { target_windows = { vim.fn.win_getid() } }<CR>",
        { noremap = true, silent = true })
      vim.api.nvim_set_keymap("x", "s",
        ":lua require('leap').leap { target_windows = { vim.fn.win_getid() } }<CR>",
        { noremap = true, silent = true })
    end
  },
}
