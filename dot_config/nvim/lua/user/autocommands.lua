return {
  {
    { "TermOpen" },
    pattern = "*",
    command = "setlocal nonumber norelativenumber",
  },
  {
    { "InsertEnter" },
    pattern = "*",
    command = "set norelativenumber",
  },
  {
    { "InsertLeave" },
    pattern = "*",
    command = "set relativenumber",
  },
  {
    { "FileType" },
    pattern = { "markdown", "tex" },
    command = "set tw=80",
  },
  {
    { "BufEnter" },
    pattern = "*",
    command = 'let @/ = ""',
  },
  {
    { "CursorHold", "CursorHoldI" },
    pattern = "*",
    command = "checktime",
  }
}
