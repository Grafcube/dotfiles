local config = {
  -- Configure AstroNvim updates
  updater = {
    channel = "stable",    -- "stable" or "nightly"
    remote = "origin",     -- remote to use
    version = "latest",    -- "latest", tag name, or regex search like "v1.*" to only do updates before v2 (STABLE ONLY)
    branch = "main",       -- branch name (NIGHTLY ONLY)
    commit = nil,          -- commit hash (NIGHTLY ONLY)
    pin_plugins = nil,     -- nil, true, false (nil will pin plugins on stable only)
    skip_prompts = false,  -- skip prompts about breaking changes
    show_changelog = true, -- show the changelog after performing an update
    auto_reload = true,    -- automatically reload and sync packer after a successful update
    auto_quit = true,      -- automatically quit the current session after a successful update
  },
  -- Set colorscheme to use
  colorscheme = "catppuccin-mocha",
  -- Diagnostics configuration (for vim.diagnostics.config({...}))
  diagnostics = {
    virtual_text = true,
    underline = true,
  },
  heirline = {
    separators = {
      left = { "", " " },
      right = { " ", "" },
    }
  },
  lsp = {
    skip_setup = {
      "rust_analyzer",
    },
    servers = {
      "gdscript",
      "racket_langserver",
      "rust_analyzer",
      "clangd",
    },
    formatting = {
      format_on_save = true,
    },
    config = {
      clangd = {
        capabilities = { offsetEncoding = "utf-8" },
      },
      rust_analyzer = {
        -- cmd = { "ra-multiplex" },
        settings = {
          ["rust-analyzer"] = {
            cargo = {
              features = "all",
            },
            check = {
              command = "clippy",
              allTargets = true,
              extraArgs = { "--all", "--", "-W", "clippy::all" },
            },
            procMacro = {
              enable = true,
            },
          },
        },
      },
      svelte = {
        settings = {
          ["svelte"] = {
            ["enable-ts-plugin"] = true,
            plugin = {
              svelte = {
                defaultScriptLanguage = "ts",
              }
            },
          }
        }
      },
      tailwindcss = {
        filetypes = {
          "css",
          "scss",
          "sass",
          "postcss",
          "html",
          "javascript",
          "javascriptreact",
          "typescript",
          "typescriptreact",
          "svelte",
          "vue",
          "rust",
        },
        settings = {
          ["tailwindCSS"] = {
            includeLanguages = { "postcss" },
          }
        },
        init_options = {
          userLanguages = {
            rust = "html",
          },
        },
      },
    },
  },
  -- This function is run last and is a good place to configuring
  -- augroups/autocommands and custom filetypes also this just pure lua so
  -- anything that doesn't fit in the normal config locations above can go here
  polish = function()
    -- Set autocommands
    for _, opts in pairs(require("user.autocommands")) do
      vim.api.nvim_create_autocmd(table.remove(opts, 1), opts)
    end
  end,
}

return config
