return {
  opt = {
    colorcolumn = "80",
    relativenumber = true,
    tabstop = 4,
    completeopt = { "menu", "menuone", "longest", "preview", "noselect" },
    ignorecase = true,
    smartcase = true,
    whichwrap = "<,>,[,],b,h,l,s",
    showcmd = true,
    cmdheight = 1,
    scrolloff = 10,
    spelllang = "en",
    foldlevel = 99,
    foldlevelstart = 99,
    foldenable = true,
    sessionoptions = "blank,buffers,curdir,folds,help,tabpages,winsize,winpos,terminal,localoptions",
  },
  g = {
    neovide_remember_window_size = true,
    better_whitespace_filetypes_blacklist = {
      "diff",
      "git",
      "gitcommit",
      "unite",
      "qf",
      "help",
      "markdown",
      "fugitive",
      "terminal",
      "toggleterm",
      "nofile",
    }
  },
  o = {
    guifont = "JetBrainsMono Nerd Font Mono:h13",
  }
}
