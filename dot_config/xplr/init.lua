version = "0.21.1"

local home = os.getenv("HOME")
local xpm_path = home .. "/.local/share/xplr/dtomvan/xpm.xplr"
local xpm_url = "https://github.com/dtomvan/xpm.xplr"

package.path = package.path
    .. ";"
    .. xpm_path
    .. "/?.lua;"
    .. xpm_path
    .. "/?/init.lua"

os.execute(
  string.format(
    "[ -e '%s' ] || git clone '%s' '%s'",
    xpm_path,
    xpm_url,
    xpm_path
  )
)

require("xpm").setup({
  plugins = {
    -- Let xpm manage itself
    "dtomvan/xpm.xplr",
    "sayanarijit/dua-cli.xplr",
    "sayanarijit/fzf.xplr",
    "prncss-xyz/icons.xplr",
    "dtomvan/ouch.xplr",
    "sayanarijit/trash-cli.xplr",
    "sayanarijit/zoxide.xplr",
    "sayanarijit/wl-clipboard.xplr",
    "sayanarijit/map.xplr",
    "sayanarijit/find.xplr",
    {
      "dtomvan/extra-icons.xplr",
      after = function()
        xplr.config.general.table.row.cols[2] = { format = "custom.icons_dtomvan_col_1" }
      end
    },
  },
  deps = { "dtomvan/xpm.xplr" },
  auto_install = true,
  auto_cleanup = true,
})

xplr.config.modes.builtin.default.key_bindings.on_key.x = {
  help = "xpm",
  messages = {
    "PopMode",
    { SwitchModeCustom = "xpm" },
  },
}

require("find").setup {
  mode = "default",
  key = "F",
  templates = {
    ["find all"] = {
      key = "a",
      find_command = "fd",
      find_args = " .",
      cursor_position = 0,
    },
    ["find files"] = {
      key = "f",
      find_command = "fd",
      find_args = " . --type file",
      cursor_position = 0,
    },
    ["find directories"] = {
      key = "d",
      find_command = "fd",
      find_args = " . --type directory",
      cursor_position = 0,
    },
  },
  refresh_screen_key = "ctrl-r",
}
