local wezterm = require 'wezterm';
local act = wezterm.action

local function table_merge(t1, t2)
    for k, v in pairs(t2) do
        if type(v) == "table" then
            if type(t1[k] or false) == "table" then
                table_merge(t1[k] or {}, t2[k] or {})
            else
                t1[k] = v
            end
        else
            t1[k] = v
        end
    end
    return t1
end

local custom_theme = wezterm.color.get_builtin_schemes()["Catppuccin Mocha"]
local overrides = {
    scrollbar_thumb = "#979eab",
    tab_bar = {
        background = "#16161d",
        active_tab = {
            fg_color = "#ffffff",
            bg_color = "#1f2227",
            intensity = "Bold",
        },
        inactive_tab = {
            fg_color = "#979eab",
            bg_color = "#16161d",
            italic = true,
        },
        new_tab = {
            bg_color = "#16161d",
            fg_color = "#16161d",
        },
    },
    foreground = "#979eab",
    background = "#16161d",
}
table_merge(custom_theme, overrides)

local function scheme_for_appearance(appearance)
    if appearance:find "Dark" then
        return "Eigenpuccin"
    else
        return "Catppuccin Latte"
    end
end

return {
    color_schemes = { Eigenpuccin = custom_theme },
    color_scheme = scheme_for_appearance(wezterm.gui.get_appearance()),
    check_for_updates = false,
    warn_about_missing_glyphs = false,
    default_cursor_style = "BlinkingBar",
    cursor_blink_rate = 250,
    cursor_blink_ease_in = "EaseIn",
    cursor_blink_ease_out = "EaseOut",
    enable_scroll_bar = true,
    hide_tab_bar_if_only_one_tab = true,
    use_fancy_tab_bar = false,
    tab_bar_at_bottom = true,
    alternate_buffer_wheel_scroll_speed = 1,
    initial_rows = 30,
    initial_cols = 120,
    window_padding = {
        left = 4,
        right = 4,
        top = 0,
        bottom = 0,
    },
    keys = {
        { key = "h",         mods = "CTRL|SHIFT", action = act { ActivateTabRelative = -1 } },
        { key = "l",         mods = "CTRL|SHIFT", action = act { ActivateTabRelative = 1 } },
        { key = "k",         mods = "CTRL|SHIFT", action = act { MoveTabRelative = -1 } },
        { key = "j",         mods = "CTRL|SHIFT", action = act { MoveTabRelative = 1 } },
        { key = 'UpArrow',   mods = 'SHIFT',      action = act.ScrollToPrompt(-1) },
        { key = 'DownArrow', mods = 'SHIFT',      action = act.ScrollToPrompt(1) },
        { key = 'PageUp',    mods = 'SHIFT',      action = act.ScrollByPage(-0.5) },
        { key = 'PageDown',  mods = 'SHIFT',      action = act.ScrollByPage(0.5) },
        { key = 'Home',      mods = 'SHIFT',      action = act.ScrollToTop },
        { key = 'End',       mods = 'SHIFT',      action = act.ScrollToBottom },
    },
    mouse_bindings = {
        -- Change the default click behavior so that it only selects
        -- text and doesn't open hyperlinks
        {
            event = { Up = { streak = 1, button = "Left" } },
            mods = "NONE",
            action = act.CompleteSelection("PrimarySelection"),
        },

        -- and make CTRL-Click open hyperlinks
        {
            event = { Up = { streak = 1, button = "Left" } },
            mods = "CTRL",
            action = act.OpenLinkAtMouseCursor,
        },

        -- Disable the 'Down' event of CTRL-Click to avoid weird program behaviors
        {
            event = { Down = { streak = 1, button = 'Left' } },
            mods = 'CTRL',
            action = act.Nop,
        }
    },
    font_size = 13.0,
    harfbuzz_features = { "calt=1", "clig=1", "liga=1" },
}
